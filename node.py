import random

import simpy

from job import Job
from log import Log
from models import NodeParams
from service_datastorage import ServiceDataStorage
from service_discovery import ServiceDiscovery

MODULE = "Node"
DEBUG = False

"""
This represents a Node who has an internal M/M/1/N queue
"""


class Node:
    def __init__(self, env: simpy.Environment or None = None,
                 node_id: int or None = None,
                 params: NodeParams or None = None,
                 discovery: ServiceDiscovery or None = None,
                 data_storage: ServiceDataStorage or None = None,
                 round_period: int or None = None,
                 round_shift: int or None = None):

        if env is None or params is None or node_id is None:
            Log.merr(MODULE, "Missing init parameters")
            exit(1)

        self._id = node_id
        """Node id"""
        self._env = env  # type: simpy.Environment
        self._params = params  # type: NodeParams
        # services
        self._sdiscovery = discovery  # type: ServiceDiscovery
        """Discovery service"""
        self._sdata_storage = data_storage  # type: ServiceDataStorage
        """Data Storage service"""
        # processes
        self._m_process_job_generator = self._env.process(self._process_job_generator())
        """Process that generates jobs at rate lambda"""
        self._m_process_node_update_migration_ratios = self._env.process(self._process_node_update_migration_ratios())
        self._m_process_node_update_last_load = self._env.process(self._process_node_update_last_load())
        """Process that generates jobs at rate lambda"""
        self._m_process_workers = []
        for i in range(self._params.n_servers):
            self._m_process_workers.append(self._env.process(self._process_job_executor()))
        """Process of job processor"""
        self._m_process_shutdown = self._env.process(self._process_node_shutdown())
        # jobs data
        # noinspection PyUnresolvedReferences
        self._queued_jobs = simpy.resources.container.Container(self._env, init=0)
        """Queued jobs container"""
        # noinspection PyUnresolvedReferences
        self._running_jobs = simpy.resources.container.Container(self._env, capacity=self._params.k, init=0)
        """Currently running jobs container"""
        self._queued_jobs_list = []  # List[Job]
        """List of queued jobs"""
        self._round_period = round_period
        """Duration of a single round in seconds"""
        self._round_shift = round_shift

        # init ratios
        self._neighbors = []
        self._migration_ratios = []

        # load function
        self._round_current_jobs = 0
        self._round_current_execution_time = 0.0
        self._round_current_arrived_jobs = 0
        self._round_current_exited_jobs = 0
        self._round_current_generated_jobs = 0

        self._round_last_jobs = 0
        self._round_last_execution_time = 0.0
        self._round_last_arrived_jobs = 0
        self._round_last_exited_jobs = 0
        self._round_last_generated_jobs = 0

        self._round_max_mi = 0.0

        self._hist_latencies = []
        self._hist_latencies_n = 10

    #
    # Processes
    #

    def _process_job_generator(self):
        """This process generates jobs towards the current node at rate lambda"""
        try:
            while True:
                # wait for generate a job
                yield self._env.timeout(random.expovariate(self._params.l_rate))
                # generate the job and take decision
                job = Job(self._env)
                self._round_current_generated_jobs += 1

                # check if cooperate according to migration ratios
                to_forward = self._is_job_to_forward()
                # Log.mdebug(f"{MODULE}#{self.get_id()}", f"[t={self._env.now}] to_forward={to_forward}")
                if to_forward >= 0:
                    self._neighbors[to_forward].schedule_job(job)
                else:
                    self._job_schedule_internally(job)

        except simpy.Interrupt:
            if DEBUG:
                print("[%d] generator interrupted" % self.get_id())

    def _process_job_executor(self):
        """This process executes available jobs for a node"""
        while True:
            # wait for a job and pick it when available
            yield self._queued_jobs.get(1)
            job = self._queued_jobs_list.pop(0)

            self._running_jobs.put(1)
            # execute it
            yield self._env.timeout(random.expovariate(self._params.mi_rate))
            self._running_jobs.get(1)

            # update execution time
            job.a_executed()
            self._job_done(job)

            # update round stats
            self._round_current_jobs += 1
            self._round_current_exited_jobs += 1
            self._round_current_execution_time += job.get_total_execution_time()

            if DEBUG:
                print("[%d] job executed: now=%d queued_jobs=%d" % (
                    self.get_id(), self._env.now, self._queued_jobs.level))

    def _process_node_update_last_load(self):
        """This process updates the migration ratios every self._round_period seconds"""
        try:
            while True:
                yield self._env.timeout(1)
                # check if we hit the round
                if self._env.now % (self._round_period - 1) == 0:
                    # Log.mdebug(f"{MODULE}#{self.get_id()}", f"[t={self._env.now}] Updating last load")
                    # update stats
                    self._round_last_jobs = self._round_current_jobs
                    self._round_last_execution_time = self._round_current_execution_time
                    self._round_last_arrived_jobs = self._round_current_arrived_jobs
                    self._round_last_exited_jobs = self._round_current_exited_jobs
                    self._round_last_generated_jobs = self._round_current_generated_jobs

                    self._round_current_jobs = 0
                    self._round_current_execution_time = 0.0
                    self._round_current_arrived_jobs = 0
                    self._round_current_exited_jobs = 0
                    self._round_current_generated_jobs = 0

                    self._round_max_mi = max(self._round_max_mi, self.get_last_mi())

                    self._hist_latencies.append(self._round_last_execution_time / self._round_last_exited_jobs)
                    if len(self._hist_latencies) > self._hist_latencies_n:
                        self._hist_latencies.pop(0)

        except simpy.Interrupt:
            if DEBUG:
                print("[%d] migration ratios updater interrupted" % self.get_id())

    def _process_node_update_migration_ratios(self):
        """This process updates the migration ratios every self._round_period seconds"""
        try:
            while True:
                yield self._env.timeout(1)
                # check if we hit the round
                if self._env.now % self._round_period == 0:
                    Log.mdebug(f"{MODULE}#{self.get_id()}", f"[t={self._env.now}] Updating ratios ("
                                                            f"arrived_jobs={self._round_last_arrived_jobs},"
                                                            f"exited_jobs={self._round_last_exited_jobs},"
                                                            f"l={self.get_last_lambda()},"
                                                            f"mi={self.get_last_mi()},"
                                                            f"ro={self.get_last_ro()}) ")

                    # retrieve the stats of all neighbors and self
                    loads = [self.get_last_latency()]
                    queue_lengths = [self.get_queue_length()]
                    rates_l = [self.get_last_lambda()]
                    rates_l_direct = [self.get_last_lambda_direct()]
                    rates_m = [self.get_last_mi()]
                    rates_m_max = [self.get_max_mi()]
                    rates_ro = [self.get_last_ro()]

                    for n in self._neighbors:
                        rates_l.append(n.get_last_lambda())
                        rates_l_direct.append(n.get_last_lambda_direct())
                        rates_m.append(n.get_last_mi())
                        rates_m_max.append(n.get_max_mi())
                        rates_ro.append(n.get_last_ro())
                        loads.append(n.get_last_latency())
                        queue_lengths.append(n.get_queue_length())

                    Log.mdebug(f"{MODULE}#{self.get_id()}", f"[t={self._env.now}] last_load={self.get_last_latency():.2f} others={loads}")
                    Log.mdebug(f"{MODULE}#{self.get_id()}", f" last_queue={self.get_queue_length()} others={queue_lengths}")
                    Log.mdebug(f"{MODULE}#{self.get_id()}", f" last_l={self.get_last_lambda():.2f} others={rates_l}")
                    Log.mdebug(f"{MODULE}#{self.get_id()}", f" last_l_direct={self.get_last_lambda_direct():.2f} others={rates_l_direct}")
                    Log.mdebug(f"{MODULE}#{self.get_id()}", f" last_mi={self.get_last_mi():.2f} others={rates_m}")
                    Log.mdebug(f"{MODULE}#{self.get_id()}", f" max_mi={self.get_max_mi():.2f} others={rates_m_max}")
                    Log.mdebug(f"{MODULE}#{self.get_id()}", f" last_ro={self.get_last_ro():.2f} others={rates_ro}")

                    ratios_total = sum(self._migration_ratios)

                    STEP_DELTA = 0.01

                    def can_be_given(how_much):
                        return round(ratios_total + how_much, 2) <= 1.0

                    def can_be_subtract(how_much):
                        return round(ratios_total - how_much, 2) > 0.0

                    def can_be_subtract_to_node(how_much, i):
                        return round(self._migration_ratios[i] - how_much, 2) > 0.0

                    # compute average latency
                    average_latency = self.get_last_latency()
                    for i, n in enumerate(self._neighbors):
                        average_latency += n.get_last_latency()
                    average_latency /= (len(self._neighbors) + 1)

                    average_latency_low = .95 * average_latency
                    average_latency_high = 1.05 * average_latency

                    def delta(to_node_j):
                        return STEP_DELTA
                        # return max(0.0, (self.get_last_latency() - average_latency) / self.get_last_latency()) * \
                        #        max(0.0, (self._neighbors[to_node_j].get_last_latency() - average_latency) / self._neighbors[to_node_j].get_last_latency()) * \
                        #        max(0.0, (self.get_last_latency() - self._neighbors[to_node_j].get_last_latency()) / self.get_last_latency()) * \
                        #       self.get_last_latency()

                    if self.get_last_latency() <= average_latency_low:
                        Log.mdebug(f"{MODULE}#{self.get_id()}", f"[t={self._env.now}] Skipping since "
                                                                f"self.get_last_latency()={self.get_last_latency()}<={average_latency_low} "
                                                                f"ratios={self._migration_ratios}")

                        # if I am giving something to others then reduce them all
                        if ratios_total > 0:
                            for i, _ in enumerate(self._migration_ratios):
                                if can_be_subtract_to_node(STEP_DELTA, i) and can_be_subtract(STEP_DELTA):
                                    self._migration_ratios[i] = round(self._migration_ratios[i] - STEP_DELTA, 2)
                                    ratios_total = round(ratios_total - STEP_DELTA, 2)
                        continue

                    # if higher than the average then I have to give others
                    if self.get_last_latency() >= average_latency_high:

                        for i, n in enumerate(self._neighbors):
                            if n.get_last_latency() >= average_latency_high:
                                if can_be_subtract_to_node(STEP_DELTA, i) and can_be_subtract(STEP_DELTA):
                                    self._migration_ratios[i] = round(self._migration_ratios[i] - STEP_DELTA, 2)
                                    ratios_total = round(ratios_total - STEP_DELTA, 2)

                            if n.get_last_latency() <= average_latency_low:
                                if can_be_given(STEP_DELTA):
                                    self._migration_ratios[i] = round(self._migration_ratios[i] + delta(i), 2)
                                    ratios_total = round(ratios_total + delta(i), 2)

                    Log.mdebug(f"{MODULE}#{self.get_id()}",
                               f"[t={self._env.now}] Ratios {self._migration_ratios} sum(ratios)={ratios_total}")

        except simpy.Interrupt:
            if DEBUG:
                print("[%d] migration ratios updater interrupted" % self.get_id())

    def _process_node_shutdown(self):
        """Process that interrupts the job generation after certain time"""
        yield self._env.timeout(self._params.simulation_time)
        self._m_process_node_update_last_load.interrupt()
        self._m_process_node_update_migration_ratios.interrupt()

        self._m_process_job_generator.interrupt()

        if DEBUG:
            Log.mdebug(MODULE, "Triggered node shutdown")

    #
    # Internals
    #

    def _job_schedule_internally(self, job: Job):
        """Mark the job as to be executed internally and add the local job queue"""
        if self.get_load() >= self._params.k:
            self._job_reject(job)
            return False

        self._round_current_arrived_jobs += 1

        job.a_queued()
        self._queued_jobs.put(1)
        self._queued_jobs_list.append(job)
        return True

    def _job_reject(self, job):
        job.a_rejected()
        self._job_done(job)

    def _job_done(self, job):
        # update data
        self._sdata_storage.record_job_done(self.get_id(), job)

    def _is_job_to_forward(self):
        random_pick = random.random()
        cumulative_ratio = 0.0
        for i, ratio in enumerate(self._migration_ratios):
            cumulative_ratio += ratio
            if random_pick < cumulative_ratio and cumulative_ratio > 0.0:
                return i
        return -1

    #
    # Exported
    #

    def get_last_latency(self):
        if len(self._hist_latencies) == 0:
            return 0.0

        return sum(self._hist_latencies) / len(self._hist_latencies)

        # if self._round_last_jobs == 0:
        #    return 0.0
        # return self._round_last_execution_time / self._round_last_exited_jobs

    def get_last_lambda_direct(self):
        return self._round_last_generated_jobs / self._round_period

    def get_last_lambda(self):
        return self._round_last_arrived_jobs / self._round_period

    def get_last_mi(self):
        if self.get_last_latency() == 0.0:
            return 0.0
        return self.get_last_lambda() + (1 / self.get_last_latency())

    def get_max_mi(self):
        return self._round_max_mi

    def get_last_ro(self):
        if self.get_max_mi() == 0.0:
            return 0.0
        return self.get_last_lambda() / self.get_max_mi()

    def get_queue_length(self):
        return self._queued_jobs.level

    def schedule_job(self, job: Job):
        """Schedule a job from external"""
        return self._job_schedule_internally(job)

    def get_load(self):
        """Retrieve the number of queued tasks"""
        return self._queued_jobs.level + self._running_jobs.level

    def get_id(self):
        """Return the identifier number of the node"""
        return self._id

    def get_migration_ratios(self):
        return self._migration_ratios

    def get_neighbors(self):
        """Returns the ids of the neighbors nodes"""
        return self._neighbors

    def get_params(self):
        return self._params

    def set_discovery_service(self, discovery: ServiceDiscovery):
        self._sdiscovery = discovery

        # init ratios
        self._neighbors = self._sdiscovery.get_neighbors_for_node(self.get_id())
        self._migration_ratios = [0.0 for _ in self._neighbors]

    def set_datastorage_service(self, data_storage: ServiceDataStorage):
        self._sdata_storage = data_storage
