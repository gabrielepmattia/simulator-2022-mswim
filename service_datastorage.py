"""
This implements a service for gathering simulation statistics
"""

import scipy
from scipy.signal import savgol_filter

from plot import PlotUtils, Plot

MODULE = "ServiceDataStorage"


class ServiceDataStorage:
    def __init__(self, nodes):
        self._nodes = nodes
        self._nodes_ids = {}
        for i, node in enumerate(nodes):
            self._nodes_ids[node.get_id()] = i

        self._jobs_total = [0 for _ in range(len(self._nodes))]
        self._jobs_rejected = [0 for _ in range(len(self._nodes))]
        self._jobs_times = [[] for _ in range(len(self._nodes))]

        self._avg_delays = {}
        self._avg_ros = {}

        self._migration_ratios = {}
        for i, node in enumerate(nodes):
            self._migration_ratios[node.get_id()] = []

        for node in self._nodes:
            self._avg_delays[node.get_id()] = []
            self._avg_ros[node.get_id()] = []

        self._pb = 0.0
        self._delay = 0.0

    #
    # Exported
    #

    def record_job_done(self, node_id: int, job):
        self._jobs_total[self._nodes_ids[node_id]] += 1
        if job.is_rejected():
            self._jobs_rejected[self._nodes_ids[node_id]] += 1
        else:
            self._jobs_times[self._nodes_ids[node_id]].append(job.get_total_execution_time())

    def record_round_end(self):
        for node in self._nodes:
            self._avg_delays[node.get_id()].append(node.get_last_latency())
            self._avg_ros[node.get_id()].append(node.get_last_ro())
            self._migration_ratios[node.get_id()].append(list(node.get_migration_ratios()))

    def plot_round_delays(self, filename, title=""):
        # PlotUtils.use_tex()
        legend = []
        to_plot_x = []
        to_plot_y = []
        for node_id in self._avg_delays.keys():
            legend.append(f"Node {node_id}")  # (mi={self._nodes[self._nodes_ids[node_id]]._params.mi_rate:.2f})")
            to_plot_x.append(list(range(len(self._avg_delays[node_id]))))
            to_plot_y.append(self._avg_delays[node_id])

        Plot.multi_plot(to_plot_x, to_plot_y, "Round", "Avg Delay", f"{filename}_delay", legend=legend, title=title)

        average_every = 15
        averaged_x = []
        averaged_y = []

        to_avg = []
        for i in range(len(to_plot_x)):
            averaged_x.append([])
            averaged_y.append([])

            for j in range(len(to_plot_x[i])):
                if j > 0 and j % average_every == 0:
                    averaged_x[i].append(j)
                    averaged_y[i].append(sum(to_avg) / len(to_avg))
                    to_avg = []
                to_avg.append(to_plot_y[i][j])

        print(to_plot_x)
        print(to_plot_y)
        print(averaged_y)
        print(averaged_x)

        Plot.multi_plot(averaged_x, averaged_y, "Round", "Avg Delay", f"{filename}_delay_avg_{average_every}", legend=legend, title=title)

        # plot filter
        to_plot_y_filtered = savgol_filter(to_plot_y, window_length=20, polyorder=4)

        # average_latency_y = []
        average_latency_y_high = []
        average_latency_y_low = []
        for i in range(len(to_plot_y_filtered[0])):
            summation = 0.0
            for latency_arr in to_plot_y_filtered:
                summation += latency_arr[i]

            average_latency = summation / len(to_plot_y_filtered)
            # average_latency_y.append(summation/len(to_plot_y_filtered))
            average_latency_y_high.append(average_latency * 1.05)
            average_latency_y_low.append(average_latency * 0.95)

        Plot.multi_plot(to_plot_x,
                        to_plot_y_filtered,
                        "Round",
                        "$d_t$ (s)",
                        f"{filename}_delay_filtered",
                        legend=legend,
                        fill_between=[average_latency_y_high, average_latency_y_low],
                        fill_between_color='gray',
                        max_w=4,
                        max_h=4,
                        linewidth=1.5,
                        label_fontsize=12,
                        use_markers=False,
                        use_grid=True,
                        )

    def plot_round_ros(self, filename, title=""):
        # PlotUtils.use_tex()
        legend = []
        to_plot_x = []
        to_plot_y = []
        for node_id in self._avg_ros.keys():
            legend.append(f"Node {node_id}")
            to_plot_x.append(list(range(len(self._avg_ros[node_id]))))
            to_plot_y.append(self._avg_ros[node_id])

        Plot.multi_plot(to_plot_x,
                        to_plot_y,
                        "Round", r"$\rho$",
                        f"{filename}_ro",
                        legend=legend,
                        title=title,
                        max_w=4,
                        max_h=4,
                        linewidth=1.5,
                        label_fontsize=12,
                        use_markers=False,
                        use_grid=True,
                        )

        to_plot_y_filtered = savgol_filter(to_plot_y, window_length=20, polyorder=4)
        Plot.multi_plot(to_plot_x,
                        to_plot_y_filtered,
                        "Round", r"$\rho$",
                        f"{filename}_ro_filter",
                        legend=legend,
                        title=title,
                        max_w=4,
                        max_h=4,
                        linewidth=1.5,
                        label_fontsize=12,
                        use_markers=False,
                        use_grid=True,
                        )

        to_plot_y_filtered = savgol_filter(to_plot_y, window_length=20, polyorder=4)
        Plot.multi_plot(to_plot_x,
                        to_plot_y_filtered,
                        "Round", r"$\rho$",
                        f"{filename}_ro_filter_rect",
                        legend=legend,
                        title=title,
                        max_w=4.5,
                        max_h=1.5,
                        linewidth=1,
                        label_fontsize=9,
                        use_markers=False,
                        use_grid=True,
                        )

    def plot_round_migration_ratios(self, filename, title=""):
        # PlotUtils.use_tex()
        for node in self._nodes:
            legend = []
            to_plot_x = []
            to_plot_y = []

            for neighbor in node.get_neighbors():
                legend.append(f"Node{neighbor.get_id()} (mi={self.get_node_by_id(neighbor.get_id()).get_params().mi_rate:.2f})")
                to_plot_x.append([])
                to_plot_y.append([])

            for i, ratios in enumerate(self._migration_ratios[node.get_id()]):
                for j, ratio in enumerate(ratios):
                    to_plot_y[j].append(ratio)
                    to_plot_x[j].append(i)

            Plot.multi_plot(to_plot_x, to_plot_y, "Round", "Ratio", f"{filename}_ratios_node{node.get_id()}", legend=legend,
                            title=f"Migration ratios for Node{node.get_id()} (mi={node.get_params().mi_rate}) (lambda={node.get_params().l_rate})")

    def plot_round_migration_ratios_sum(self, filename, title=""):
        PlotUtils.use_tex()
        to_plot_x = []
        to_plot_y = []
        legend = []

        for i, node in enumerate(self._nodes):
            legend.append(f"$\sum_j m_{{ {i}j }}$")
            to_plot_x.append([])
            to_plot_y.append([])

            for j, ratios in enumerate(self._migration_ratios[node.get_id()]):
                sum_ratios = 0.0

                for _, ratio in enumerate(ratios):
                    sum_ratios += ratio

                to_plot_x[i].append(j)
                to_plot_y[i].append(sum_ratios)

        Plot.multi_plot(to_plot_x,
                        to_plot_y,
                        "Round", "Migration Ratio",
                        f"{filename}_ratios_sum", legend=legend,
                        max_w=4,
                        max_h=4,
                        linewidth=1.5,
                        label_fontsize=12,
                        use_markers=False,
                        use_grid=True
                        )

    def plot_round_migration_ratios_all(self, discovery_service, filename, title=""):
        print(f"plot_round_migration_ratios_all: {filename}")

        PlotUtils.use_tex()
        to_plot_x = []
        to_plot_y = []
        legend = []

        for i, node in enumerate(self._nodes):
            neighbours = discovery_service.get_neighbors_for_node(i)
            print(f"Node {i} has {len(neighbours)} neighbours")

            neighbours_ids = []
            to_plot_id_start = len(to_plot_x)

            for neighbour in neighbours:
                legend.append(f"$m_{{ {i} {neighbour.get_id()} }}$")
                neighbours_ids.append(neighbour.get_id())

                to_plot_x.append([])
                to_plot_y.append([])

            for t, ratios in enumerate(self._migration_ratios[node.get_id()]):
                for node_j, ratio in enumerate(ratios):
                    # print(f"self._migration_ratios[{node.get_id()}] = {self._migration_ratios[node.get_id()]}")

                    to_plot_x[to_plot_id_start + node_j].append(t)
                    to_plot_y[to_plot_id_start + node_j].append(ratio)

        print(f"len(to_plot_x) = {len(to_plot_x)}")
        for i in range(len(to_plot_x)):
            print(f"-> len(to_plot_x)[{i}] = {len(to_plot_x[i])}: {to_plot_x[i]}")

        print(f"len(to_plot_y) = {len(to_plot_y)}")
        for j in range(len(to_plot_y)):
            print(f"-> len(to_plot_y)[{j}] = {len(to_plot_y[j])}: {to_plot_y[j]}")

        Plot.multi_plot(to_plot_x,
                        to_plot_y,
                        "Round", "Migration Ratio",
                        f"{filename}_ratios_all", legend=legend,
                        max_w=4,
                        max_h=4,
                        linewidth=1.5,
                        label_fontsize=12,
                        use_markers=False,
                        use_grid=True
                        )

    def plot_round_delays_difference(self, filename, title=""):
        # PlotUtils.use_tex()
        nodes = list(self._avg_delays.keys())
        to_plot_x = list(range(len(self._avg_delays[nodes[0]])))
        to_plot_y = []

        for r in to_plot_x:
            to_plot_y.append(0)
            for node_i in nodes:
                if node_i == 0:
                    to_plot_y[r] += self._avg_delays[nodes[node_i]][r]
                else:
                    to_plot_y[r] -= self._avg_delays[nodes[node_i]][r]
            to_plot_y[r] = abs(to_plot_y[r])

        Plot.plot(to_plot_x, to_plot_y, "Round", "Avg Diff Delay", f"{filename}_delay_diff", title=title)

    def print_data(self):
        print()
        print("==== STATS ====")
        average_pb = 0.0
        average_delay = 0.0

        print(self._jobs_total)
        print(self._jobs_rejected)

        for i in range(len(self._nodes)):
            average_pb += (self._jobs_rejected[i] / self._jobs_total[i])

            if len(self._jobs_times[i]) > 0:
                tmp_delay = 0.0
                for time in self._jobs_times[i]:
                    tmp_delay += time
                average_delay += (tmp_delay / (self._jobs_total[i] - self._jobs_rejected[i]))

        average_pb /= len(self._nodes)
        average_delay /= len(self._nodes)

        self._pb = average_pb
        self._delay = average_delay

        print("avg_total_job=%.3f" % (sum(self._jobs_total) / len(self._nodes)))
        print("pb=%.7f" % average_pb)
        print("delay=%.3f" % average_delay)

    def get_pb(self):
        return self._pb

    def get_delay(self):
        return self._delay

    #
    # Utils
    #

    def get_node_by_id(self, node_id):
        return self._nodes[self._nodes_ids[node_id]]
