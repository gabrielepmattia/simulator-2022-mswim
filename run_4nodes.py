import math
import sys

import simpy

from log import Log
from models import NodeParams
from node import Node
from plot import PlotUtils
from service_datastorage import ServiceDataStorage
from service_discovery import ServiceDiscovery

"""
Run the simulation 
"""

MODULE = "Simulate"

SIMULATION_TIME = 20000
SIMULATION_TOTAL_TIME = SIMULATION_TIME + 100
NODES = 3  # number of nodes
ROUND_PERIOD = 60  # seconds

TOPOLOGY = "star"


def simulate(env):
    Log.minfo(MODULE, "Initializing nodes and parameters")
    Log.mdebug(MODULE, "total_time=%d, n_nodes=%d" % (SIMULATION_TOTAL_TIME, NODES))

    nodes = []
    params = NodeParams()
    params.simulation_time = SIMULATION_TIME
    params.n_servers = 1
    params.l_rate = 1
    params.mi_rate = 2
    params.k = 4
    nodes.append(Node(node_id=0, env=env, params=params, round_period=ROUND_PERIOD))

    params = NodeParams()
    params.simulation_time = SIMULATION_TIME
    params.n_servers = 1
    params.l_rate = 1.5
    params.mi_rate = 1.5
    params.k = 4
    nodes.append(Node(node_id=1, env=env, params=params, round_period=ROUND_PERIOD))

    params = NodeParams()
    params.simulation_time = SIMULATION_TIME
    params.n_servers = 1
    params.l_rate = 4.0
    params.mi_rate = 2.5
    params.k = 4
    nodes.append(Node(node_id=2, env=env, params=params, round_period=ROUND_PERIOD))

    params = NodeParams()
    params.simulation_time = SIMULATION_TIME
    params.n_servers = 1
    params.l_rate = 3.0
    params.mi_rate = 1
    params.k = 4
    nodes.append(Node(node_id=3, env=env, params=params, round_period=ROUND_PERIOD))

    # add them discovery service
    discovery = ServiceDiscovery(nodes)
    data_storage = ServiceDataStorage(nodes)

    if TOPOLOGY == "ring":
        discovery.set_neighbors_for_node(0, [3, 1])
        discovery.set_neighbors_for_node(1, [0, 2])
        discovery.set_neighbors_for_node(2, [1, 3])
        discovery.set_neighbors_for_node(3, [2, 0])

    if TOPOLOGY == "star":
        discovery.set_neighbors_for_node(0, [1, 2, 3])
        discovery.set_neighbors_for_node(1, [0])
        discovery.set_neighbors_for_node(2, [0])
        discovery.set_neighbors_for_node(3, [0])

    # add services in them
    for node in nodes:
        node.set_discovery_service(discovery)
        node.set_datastorage_service(data_storage)

    # info process
    def process_info(env):
        while True:
            yield env.timeout(1)
            total_blocks = 30
            progress = (env.now / SIMULATION_TOTAL_TIME) * 100
            progress_blocks = int(math.ceil(progress * total_blocks / 100))
            print("\r[Simulate] Progress [" + "=" * progress_blocks + " " * (
                    total_blocks - progress_blocks) + "] %.2f%% [t=%d]" % (
                      progress, env.now), end="")

    def process_info_round(env):
        round_number = 0
        nodes = discovery.get_nodes()
        while True:
            yield env.timeout(1)
            if env.now % ROUND_PERIOD == 0:
                print(f"[Simulate] Round#{round_number} [t={env.now}] | ", end="")
                for node_id in nodes.keys():
                    print(f"{node_id}={round(nodes[node_id].get_last_latency(), 4)}", end=" ")
                print()

                data_storage.record_round_end()
                round_number += 1

    # env.process(process_info(env))
    env.process(process_info_round(env))

    Log.minfo(MODULE, "Starting simulation")
    env.run(until=SIMULATION_TOTAL_TIME)

    data_storage.print_data()

    PlotUtils.use_tex()
    data_storage.plot_round_delays(f"4_nodes_{TOPOLOGY}")
    data_storage.plot_round_delays_difference(f"4_nodes_{TOPOLOGY}")
    data_storage.plot_round_ros(f"4_nodes_{TOPOLOGY}")
    data_storage.plot_round_migration_ratios(f"4_nodes_{TOPOLOGY}")
    data_storage.plot_round_migration_ratios_sum(f"4_nodes_{TOPOLOGY}")
    data_storage.plot_round_migration_ratios_all(discovery, f"4_nodes_{TOPOLOGY}")


def main(argv):
    env = simpy.Environment()
    simulate(env)


if __name__ == "__main__":
    main(sys.argv)
