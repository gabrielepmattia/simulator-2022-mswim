import os

import matplotlib.pyplot as plt

PLOT_DIRECTORY = "plot"
DATA_DIRECTORY = "raw"

markers = [r"$\triangle$", r"$\square$", r"$\diamondsuit$", r"$\otimes$", r"$\star$"]


# markers = ["o", "x", "*"]


class PlotUtils:
    """Plot utilities"""

    @staticmethod
    def use_tex():
        plt.rcParams.update({
            'font.family': 'serif',
            'text.usetex': True,
            'text.latex.preamble': ""
                                   # r"\DeclareUnicodeCharacter{03BB}{$\lambda$}"
                                   # + r"\DeclareUnicodeCharacter{03BC}{$\mu$}"
                                   # + r"\usepackage[utf8]{inputenc}"
                                   + r"\usepackage{amssymb}"
                                   + r"\usepackage{amsmath}"
            # + r"\usepackage[libertine]{newtxmath}"
            # + r"\usepackage[libertine]{newtxmath}\usepackage[T1]{fontenc}"
            # + r"\usepackage{mathptmx}"
            # + r"\usepackage[T1]{fontenc}"
        })
        return True


class Plot:
    @staticmethod
    def plot(x_arr, y_arr, x_label, y_label, filename, title=None, log=False):
        plt.clf()
        fig, ax = plt.subplots()
        line_experimental, = ax.plot(x_arr, y_arr, marker="x", markersize=3.0, markeredgewidth=1, linewidth=0.7)

        if title is not None:
            ax.set_title(title)

        if log:
            ax.set_yscale('log')

        ax.set_xlabel(x_label)
        ax.set_ylabel(y_label)
        fig.tight_layout()
        os.makedirs(PLOT_DIRECTORY, exist_ok=True)
        plt.savefig("{}/{}.pdf".format(PLOT_DIRECTORY, filename))
        plt.close(fig)

    @staticmethod
    def multi_plot(x_arr_arr, y_arr_arr, x_label, y_label, filename, legend=None, title=None, log=False, xlim=None,
                   ylim=None, legend_position=None, max_w=6.4, max_h=4.8, use_grid=True, use_markers=False, linewidth=1.2,
                   label_fontsize=10, fill_between=None, fill_between_color='blue'):
        if len(x_arr_arr) != len(y_arr_arr):
            print("Size mismatch")
            return

        plt.clf()
        fig, ax = plt.subplots()
        legend_arr = []

        for i in range(len(y_arr_arr)):
            if use_markers:
                line, = ax.plot(x_arr_arr[i], y_arr_arr[i],
                                markerfacecolor='None', linewidth=linewidth,
                                marker=markers[i % len(markers)],
                                markersize=5,
                                markeredgewidth=0.6
                                )
            else:
                line, = ax.plot(x_arr_arr[i], y_arr_arr[i],
                                linewidth=linewidth
                                )

            if log:
                ax.set_yscale('log')
            if xlim is not None:
                ax.set_xlim(xlim)
            if ylim is not None:
                ax.set_ylim(ylim)
            if legend is not None:
                legend_arr.append(line)

        if legend_position is not None:
            plt.rcParams["legend.loc"] = str(legend_position)

        if legend is not None and len(legend) == len(legend_arr):
            plt.legend(legend_arr, legend, fontsize=label_fontsize-1)

        if use_grid:
            plt.grid(color='#cacaca', linestyle='--', linewidth=0.5)

        if title is not None:
            ax.set_title(title)

        if fill_between is not None:
            ax.fill_between(x_arr_arr[0], fill_between[0], fill_between[1], alpha=0.2, facecolor=fill_between_color)

        # ax.set_title(title)
        ax.set_xlabel(x_label, fontsize=label_fontsize)
        ax.set_ylabel(y_label, fontsize=label_fontsize)

        plt.xticks(fontsize=label_fontsize - 1)
        plt.yticks(fontsize=label_fontsize - 1)

        fig.tight_layout()

        fig.set_figwidth(max_w)
        fig.set_figheight(max_h)

        os.makedirs(PLOT_DIRECTORY, exist_ok=True)
        plt.savefig("{}/{}.pdf".format(PLOT_DIRECTORY, filename), bbox_inches='tight')
        plt.close(fig)

    @staticmethod
    def multi_save_data(x_arr_arr, y_arr_arr, filename="", legend=None):
        if len(x_arr_arr) != len(y_arr_arr):
            print("Size mismatch")
            return

        os.makedirs(DATA_DIRECTORY, exist_ok=True)
        file_open = open("{}/{}.txt".format(DATA_DIRECTORY, filename), "w")

        if legend is not None:
            out = "# x"
            for label in legend:
                out += " "
                out += str(label)
            print(out, file=file_open)

        for i in range(len(x_arr_arr[0])):
            out = "{:.3f}".format(x_arr_arr[0][i])
            for j in range(len(y_arr_arr)):
                out += " "
                out += "{:.7f}".format(y_arr_arr[j][i])
            print(out, file=file_open)

        file_open.close()

    @staticmethod
    def save_data(x_arr, y_arr, filename=""):
        if len(x_arr) != len(y_arr):
            print("Size mismatch")
            return

        os.makedirs(DATA_DIRECTORY, exist_ok=True)
        file_open = open("{}/{}.txt".format(DATA_DIRECTORY, filename), "w")

        for i in range(len(x_arr)):
            print("%.2f %.7f" % (x_arr[i], y_arr[i]), file=file_open)

        file_open.close()
