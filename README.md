# simulator-2022-mswim

Simulator code of the paper "A latency-levelling load balancing algorithm for Fog and Edge Computing"

[https://dx.doi.org/10.1145/3551659.3559048](https://dx.doi.org/10.1145/3551659.3559048)